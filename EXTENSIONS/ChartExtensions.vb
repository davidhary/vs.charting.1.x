﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace ChartingExtensions

    ''' <summary> Includes extensions for charts. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/22/2014" by="David" revision="1.0.5168.x"> Created </history>
    Public Module Methods

#Region " CHART POINTS "

        ''' <summary> Remove all values from a collection. </summary>
        ''' <remarks> Same as clear. Is preferred in some collections such as the collection of points in a
        ''' chart where clear is slow. </remarks>
        ''' <param name="collection"> The collection. </param>
        <Extension()>
        Public Sub RemoveAll(Of T)(ByVal collection As ObjectModel.Collection(Of T))
            If collection IsNot Nothing AndAlso collection.Count > 0 Then
                Do While collection.Count > 0
                    collection.RemoveAt(collection.Count - 1)
                Loop
            End If
        End Sub

        ''' <summary> Fast clear. </summary>
        ''' <param name="points"> The points. </param>
        ''' <remarks> From Code Project Speedup MS Chart Clear Data Points by Code Artist, 30 Apr 2012, 
        ''' http://www.codeproject.com/Articles/376060/Speedup-MSChart-Clear-Data-Points
        ''' </remarks>
        <Extension()>
        Public Sub FastClear(ByVal points As DataVisualization.Charting.DataPointCollection)
            If points IsNot Nothing AndAlso points.Count > 0 Then
                points.SuspendUpdates()
                points.RemoveAll()
                points.ResumeUpdates()
                ' This ensures the chart axis updates correctly on next plot. 
                ' Without adding this line, previous axis settings is used  
                ' when plotting new data points right after calling Fast Clear.
                points.Clear()
            End If
        End Sub

#End Region

    End Module

End Namespace
