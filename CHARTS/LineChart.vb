﻿Imports System.Drawing
Imports System.Windows.Forms
Imports System.Windows.Forms.DataVisualization.Charting
''' <summary> A line chart. </summary>
''' <remarks> David, 7/29/2016. </remarks>
''' <license>
''' (c) 2016 Darryl Bryk. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="7/29/2016" by="David" revision=""> 
''' http://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series. </history>
Public Class LineChart
    Implements IDisposable

#Region " CONSTRUCTORS "

    Public Sub New(ByVal control As Control)
        MyBase.New()
        Me._Chart = New Chart()
        Me._ChartContainer = control
        Me.ChartContainer.Controls.Add(Chart)
        Me.Initialize()
    End Sub

#Region "IDisposable Support"

    ''' <summary> Gets or sets the disposed. </summary>
    ''' <value> The disposed. </value>
    Protected ReadOnly Property Disposed As Boolean

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed Then
                If disposing Then
                    If Me._ChartArea IsNot Nothing Then
                        Me._ChartArea.Dispose()
                        Me._ChartArea = Nothing
                    End If
                    If Me._Chart IsNot Nothing Then
                        Me._ChartContainer.Controls.Remove(Me._Chart)
                        Me._Chart.Dispose()
                        Me._Chart = Nothing
                    End If
                    Me._ChartContainer = Nothing
                End If
                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TODO: set large fields to null.
            End If
        Finally
            Me._Disposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Allows an object to try to free resources and perform other cleanup operations before it is
    ''' reclaimed by garbage collection.
    ''' </summary>
    ''' <remarks> override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        MyBase.Finalize()
    End Sub


    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region
#End Region

#Region " CHART ITEM NAMES "

    ''' <summary> Name of the chart legend. </summary>
    Public Const ChartLegendName As String = "Legend"

    ''' <summary> Name of the chart area. </summary>
    Public Const ChartAreaName As String = "Area"

#End Region

#Region " CONTROL ITEMS "

    ''' <summary> Gets the chart container. </summary>
    ''' <value> The chart container. </value>
    Public ReadOnly Property ChartContainer() As Control

    ''' <summary> Gets the chart. </summary>
    ''' <value> The chart. </value>
    Public ReadOnly Property Chart As Chart

    ''' <summary> Gets the chart area. </summary>
    ''' <value> The chart area. </value>
    Public ReadOnly Property ChartArea As ChartArea

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 7/30/2016. </remarks>
    Private Sub Initialize()

        Me.Chart.Dock = DockStyle.Fill ' Size to the control
        AddHandler Me.Chart.GetToolTipText, AddressOf Me.Chart_GetToolTipText

        Dim leg As Legend = Me.Chart.Legends.Add(LineChart.ChartLegendName)
        leg.Docking = Docking.Bottom
        leg.Font = New Font(Chart.Legends(0).Font.FontFamily, 8) ' Font size
        leg.IsTextAutoFit = True

        Me.Chart.Palette = ChartColorPalette.None
        Me.Chart.PaletteCustomColors = New Color() {Color.Blue, Color.DarkRed, Color.OliveDrab,
                                                    Color.DarkOrange, Color.Purple, Color.Turquoise,
                                                    Color.Green, Color.Crimson, Color.RoyalBlue,
                                                    Color.Sienna, Color.Teal, Color.YellowGreen,
                                                    Color.SandyBrown, Color.DeepSkyBlue,
                                                    Color.Brown, Color.Cyan}

        Me._ChartArea = Me.Chart.ChartAreas.Add(LineChart.ChartAreaName)

        ' Enable range selection and zooming
        Me.ChartArea.CursorX.IsUserEnabled = True
        Me.ChartArea.CursorX.IsUserSelectionEnabled = True
        Me.ChartArea.AxisX.ScaleView.Zoomable = True
        Me.ChartArea.AxisX.ScrollBar.IsPositionedInside = True
        Me.ChartArea.AxisX.ScaleView.SmallScrollSize = 1
        Me.ChartArea.AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount
        Me.ChartArea.AxisY.IntervalAutoMode = IntervalAutoMode.FixedCount

        Me.ChartArea.AxisX.LabelStyle.Format = "F0"
        Me.ChartArea.AxisX.LabelStyle.IsEndLabelVisible = True
        Me.ChartArea.AxisX.MajorGrid.LineColor = Color.FromArgb(200, 200, 200)

        Me.ChartArea.AxisY.MajorGrid.LineColor = Color.FromArgb(200, 200, 200)
        Me.ChartArea.AxisX.LabelStyle.Font = New Font(Me.ChartArea.AxisX.LabelStyle.Font.Name, 9, FontStyle.Regular)
        Me.ChartArea.AxisY.LabelStyle.Font = New Font(Me.ChartArea.AxisY.LabelStyle.Font.Name, 9, FontStyle.Regular)
        Me.ChartArea.AxisX.TitleFont = New Font(Me.ChartArea.AxisX.TitleFont.Name, 9, FontStyle.Regular)
        Me.ChartArea.AxisY.TitleFont = New Font(Me.ChartArea.AxisY.TitleFont.Name, 9, FontStyle.Regular)

    End Sub

#End Region

    ''' <summary> Number of series in the chart. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <returns> An Integer. </returns>
    Public Function SeriesCount() As Integer
        Return Me.Chart.Series.Count
    End Function

    ''' <summary> Add graph title. Repeated calls append new titles if unique or if new position. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="value"> The title. </param>
    Public Sub Title(ByVal value As String)
        ' Defaults to top, black
        Me.Title(value, Docking.Top, Color.Black)
    End Sub

    ''' <summary>
    ''' Add graph title. Repeated calls append new titles if unique or if new position.
    ''' </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="value">    The title. </param>
    ''' <param name="position"> The position. </param>
    Public Sub Title(ByVal value As String, ByVal position As Docking)
        Me.Title(value, position, Color.Black)
    End Sub

    ''' <summary>
    ''' Add graph title. Repeated calls append new titles if unique or if new position.
    ''' </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="value">      The title. </param>
    ''' <param name="position">   The position. </param>
    ''' <param name="titleColor"> The title color. </param>
    Public Sub Title(ByVal value As String, ByVal position As Docking, ByVal titleColor As Color)
        For Each t As Title In Me.Chart.Titles
            If t.Docking = position Then
                If Not t.Text.Contains(value) Then ' Append
                    t.Text = $"{t.Text}{vbLf}{value}"
                End If
                Return
            End If
        Next t

        ' If here, no match, add new title
        Dim newtitle As New Title(value, position)
        Me.Chart.Titles.Add(newtitle)
        newtitle.ForeColor = titleColor
        'newtitle.Font = new Font(newtitle.Font.Name, 8, FontStyle.Bold);
    End Sub

    ''' <summary> Axis x coordinate title. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="value"> The title. </param>
    Public Sub AbscissaTitle(ByVal value As String)
        Me.ChartArea.AxisX.Title = value
    End Sub

    ''' <summary> Axis y coordinate title. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="value"> The title. </param>
    Public Sub OrdinateTitle(ByVal value As String)
        If Me.ChartArea.AxisY.Title = "" Then
            Me.ChartArea.AxisY.Title = value
        ElseIf Not ChartArea.AxisY.Title.Contains(value) Then
            Me.ChartArea.AxisY.Title = $"{Me.ChartArea.AxisY.Title}, {value}" ' Append
        End If
    End Sub


    ''' <summary> Adds line series to chart.. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="seriesName"> The series name. </param>
    ''' <param name="abscissaValues">         The x coordinate values. </param>
    ''' <param name="ordinateValues">         The y coordinate values. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphLineSeries(ByVal seriesName As String, ByVal abscissaValues() As Double, ByVal ordinateValues() As Double) As Series
        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        Return GraphLineSeries(seriesName, abscissaValues, ordinateValues, False, Color.Empty, True)
    End Function

    ''' <summary> Adds line series to chart. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="seriesName">      The series name. </param>
    ''' <param name="abscissaValues">  The x coordinate values. </param>
    ''' <param name="ordinateValues">  The y coordinate values. </param>
    ''' <param name="title">           The title. </param>
    ''' <param name="abscissaTitle">   The abscissa title. </param>
    ''' <param name="ordinateTitle">   The ordinate title. </param>
    ''' <param name="addMinMaxLabels"> true to add minimum and maximum labels. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphLineSeries(ByVal seriesName As String, ByVal abscissaValues() As Double, ByVal ordinateValues() As Double,
                                    ByVal title As String, ByVal abscissaTitle As String, ByVal ordinateTitle As String,
                                    ByVal addMinMaxLabels As Boolean) As Series
        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        Me.Title(title, Docking.Top)
        Me.OrdinateTitle(ordinateTitle)
        Me.AbscissaTitle(abscissaTitle)
        Return GraphLineSeries(seriesName, abscissaValues, ordinateValues, addMinMaxLabels, Color.Empty, True)
    End Function

    ''' <summary> Adds line series to chart. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="seriesName">        The series name. </param>
    ''' <param name="abscissaValues">    The x coordinate values. </param>
    ''' <param name="ordinateValues">    The y coordinate values. </param>
    ''' <param name="addMinMaxLabels">   true to add minimum and maximum labels. </param>
    ''' <param name="seriesColor">       The series color. </param>
    ''' <param name="isVisibleInLegend"> true if this object is visible in legend. </param>
    ''' <returns> The Series. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Function GraphLineSeries(ByVal seriesName As String, ByVal abscissaValues() As Double,
                                    ByVal ordinateValues() As Double,
                                    ByVal addMinMaxLabels As Boolean, ByVal seriesColor As Color,
                                    ByVal isVisibleInLegend As Boolean) As Series

        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        Dim ser As New Series(seriesName)

        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Me.Chart.Series.Add(ser)
            ser.ChartType = SeriesChartType.Line
            If Me.Chart.ChartAreas.Count() > 1 Then ' Bind to last chart area
                ser.ChartArea = Me.Chart.ChartAreas(Chart.ChartAreas.Count() - 1).Name
            End If
            ser.Points.DataBindXY(abscissaValues, ordinateValues)
        Catch e1 As ArgumentException ' Handle redundant series
            Return Nothing
        Catch
            Throw
        End Try

        ser.IsVisibleInLegend = isVisibleInLegend
        ser.Color = seriesColor

        If addMinMaxLabels Then ' Add min, max labels
            Me.Chart.ApplyPaletteColors() ' Force color assign so labels match
            LineChart.SmartLabels(ser, ser.Color, ser.Color, FontStyle.Regular, 7)
            ser.SmartLabelStyle.MovingDirection = LabelAlignmentStyles.TopLeft

            Dim p As DataPoint = ser.Points.FindMaxByValue()
            p.Label = "Max = " & p.YValues(0).ToString("F1")
            p = ser.Points.FindMinByValue()
            p.Label = "Min = " & p.YValues(0).ToString("F1")
        End If

        System.Windows.Forms.Cursor.Current = Cursors.Default

        Return ser
    End Function

    ''' <summary> Graph range series. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="seriesName">           The series name. </param>
    ''' <param name="abscissaValues">       The x coordinate values. </param>
    ''' <param name="ordinateValues">       The y coordinate values. </param>
    ''' <param name="ordinateHeightValues"> The ordinate height values. </param>
    ''' <param name="seriesColor">          The series color. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphRangeSeries(ByVal seriesName As String, ByVal abscissaValues() As Double, ByVal ordinateValues() As Double,
                                     ByVal ordinateHeightValues() As Double, ByVal seriesColor As Color) As Series
        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        If ordinateHeightValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateHeightValues))
        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Dim ser As New Series(seriesName)

        Try
            Me.Chart.Series.Add(ser)
            If Me.Chart.ChartAreas.Count() > 1 Then ' Bind to last chart area
                ser.ChartArea = Me.Chart.ChartAreas(Me.Chart.ChartAreas.Count() - 1).Name
            End If
            ser.ChartType = SeriesChartType.Range
            ser.IsVisibleInLegend = False
            ser.Color = seriesColor

            ser.Points.DataBindXY(abscissaValues, ordinateValues, ordinateHeightValues)
        Catch e1 As ArgumentException
            ' Handles redundant series
            Return Nothing
        Catch
            Throw
        End Try
        System.Windows.Forms.Cursor.Current = Cursors.Default

        Return ser
    End Function

    ''' <summary> Adds t=0 line annotation to chart. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    Public Sub GraphOriginLine()
        GraphLineAnnotations("t=0", 0, Color.Black, True, ChartDashStyle.Dash)
    End Sub

    Public Sub GraphLineAnnotations(ByVal name As String, ByVal anchorX As Double,
                                    ByVal lineColor As Color,
                                    labelEnabled As Boolean)
        Me.GraphLineAnnotations(name, anchorX, lineColor, labelEnabled, ChartDashStyle.Solid)
    End Sub

    Public Sub GraphLineAnnotations(ByVal name As String, ByVal anchorX As Double,
                                    ByVal lineColor As Color,
                                    lineStyle As ChartDashStyle)
        Me.GraphLineAnnotations(name, anchorX, lineColor, False, lineStyle)
    End Sub

    ''' <summary> Adds line annotation to chart. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="anchorX">      The x coordinate. </param>
    ''' <param name="lineColor">    The lineColor. </param>
    ''' <param name="labelEnabled"> (Optional) true to label. </param>
    ''' <param name="lineStyle">    (Optional) the line style. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub GraphLineAnnotations(ByVal name As String, ByVal anchorX As Double,
                                    ByVal lineColor As Color, ByVal labelEnabled As Boolean,
                                    ByVal lineStyle As ChartDashStyle)
        For Each ann As Annotation In Me.Chart.Annotations
            If ann.Name = name Then ' Don't duplicate
                Return
            End If
        Next ann

        Dim annot As New VerticalLineAnnotation()
        annot.AxisX = Me.ChartArea.AxisX
        annot.AxisY = Me.ChartArea.AxisY

        annot.IsInfinitive = True
        annot.ClipToChartArea = Me.ChartArea.Name
        annot.LineWidth = 1
        annot.LineColor = lineColor
        annot.LineDashStyle = lineStyle
        annot.ToolTip = name
        annot.Name = annot.ToolTip
        annot.AnchorX = anchorX
        Me.Chart.Annotations.Add(annot)

        If labelEnabled Then
            Dim a As New RectangleAnnotation()
            a.Text = name
            a.AxisX = annot.AxisX
            a.AxisY = annot.AxisY
            a.AnchorX = anchorX
            a.LineColor = Color.Transparent
            a.BackColor = Color.Transparent
            a.ForeColor = lineColor

            Dim textw As Double = TextRenderer.MeasureText(name, a.Font).Width + 1.0F
            Dim texth As Double = TextRenderer.MeasureText(name, a.Font).Height + 1.0F
            a.Width = (textw / Me.Chart.Width) * 100
            a.Height = (texth / Me.Chart.Height) * 100
            a.X = a.X - Math.Floor(0.5 * a.Width)
            Me.ChartArea.RecalculateAxesScale() ' Needed so AxisY.Maximum != NaN
            a.Y = Me.ChartArea.AxisY.Maximum
            Me.Chart.Annotations.Add(a)

            ' Handle resize - annotation sized as % of chart size
            AddHandler Me.Chart.Resize, Sub(sender, e)
                                            a.Width = (textw / Me.Chart.Width) * 100
                                            a.Height = (texth / Me.Chart.Height) * 100
                                        End Sub
        End If
    End Sub

    ''' <summary> Graphs series points style to chart. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="seriesName">        Name of the series. </param>
    ''' <param name="abscissaValues">          The abscissa values. </param>
    ''' <param name="ordinateValues">    The ordinate values. </param>
    ''' <param name="markerStyle">       The marker style. </param>
    ''' <param name="markerSize">        Size of the marker. </param>
    ''' <param name="isVisibleInLegend"> true if this object is visible in legend. </param>
    ''' <returns> The Series. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Function GraphPoints(ByVal seriesName As String, ByVal abscissaValues() As Double,
                                ByVal ordinateValues() As Double, ByVal markerStyle As MarkerStyle,
                                ByVal markerSize As Integer, ByVal isVisibleInLegend As Boolean) As Series
        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        Dim ser As New Series(seriesName)
        If Me.Chart.Series.IsUniqueName(seriesName) Then
            Me.Chart.Series.Add(ser)
        Else
            ser = Me.Chart.Series.Item(seriesName)
        End If

        ser.ChartType = SeriesChartType.Point
        ser.IsVisibleInLegend = isVisibleInLegend
        'ser.MarkerBorderWidth = LineWeight;
        'ser.BorderWidth = LineWeight;
        ser.Points.DataBindXY(abscissaValues, ordinateValues)
        ser.MarkerStyle = markerStyle
        ser.MarkerSize = markerSize ' Points

        Return ser
    End Function

    ''' <summary> Setup Smart labels. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="chartingSeries"> The charting series. </param>
    ''' <param name="labelColor">     The label color. </param>
    ''' <param name="lineColor">      The lineColor. </param>
    ''' <param name="fontStyle">      The font style. </param>
    ''' <param name="fontSize">       (Optional) size of the font. </param>
    Private Shared Sub SmartLabels(ByVal chartingSeries As Series, ByVal labelColor As Color,
                                   ByVal lineColor As Color, ByVal fontStyle As FontStyle,
                                   ByVal fontSize As Integer)
        If chartingSeries Is Nothing Then Throw New ArgumentNullException(NameOf(chartingSeries))

        chartingSeries.LabelForeColor = labelColor
        chartingSeries.Font = New Font(chartingSeries.Font.Name, fontSize, fontStyle)
        chartingSeries.SmartLabelStyle.Enabled = True
        chartingSeries.SmartLabelStyle.CalloutLineColor = lineColor
        chartingSeries.SmartLabelStyle.CalloutStyle = LabelCalloutStyle.None
        chartingSeries.SmartLabelStyle.IsMarkerOverlappingAllowed = False
        chartingSeries.SmartLabelStyle.MinMovingDistance = 1
        chartingSeries.SmartLabelStyle.IsOverlappedHidden = False
        chartingSeries.SmartLabelStyle.AllowOutsidePlotArea = LabelOutsidePlotAreaStyle.No
        chartingSeries.SmartLabelStyle.CalloutLineAnchorCapStyle = LineAnchorCapStyle.None
    End Sub

    ''' <summary> Event handler. Called by chart for tool tip events. </summary>
    ''' <remarks> David, 7/29/2016. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tool tip event information. </param>
    Private Sub Chart_GetToolTipText(ByVal sender As Object, ByVal e As ToolTipEventArgs)
        If e Is Nothing Then Return
        Dim h As HitTestResult = e.HitTestResult
        Select Case h.ChartElementType
            Case ChartElementType.Axis, ChartElementType.AxisLabels
                e.Text = "Click-drag in graph area to zoom"
            Case ChartElementType.ScrollBarZoomReset
                e.Text = "Zoom undo"
            Case ChartElementType.DataPoint
                e.Text = $"{h.Series.Name}{ControlChars.Lf}{h.Series.Points.Item(h.PointIndex)}"
        End Select
    End Sub

End Class
